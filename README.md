# Personal OKRs

This project tracks my quarterly goals in the form of [OKRs](https://en.wikipedia.org/wiki/OKR). I sometimes also create less formal monthly goals or task lists.

View them in the [issues](https://gitlab.com/abrams-career-development/personal-okrs/-/issues).
